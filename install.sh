#!/bin/sh

[ -n "$WINEPREFIX" ] || WINEPREFIX="$HOME/.wine"
wine64 regsvr32 $PWD/wineasio.dll.so
cp wineasio.dll.so "$WINEPREFIX/drive_c/windows/system32/wineasio.dll"
