#!/bin/sh

set -e

wget -N http://www.steinberg.net/sdk_downloads/ASIOSDK2.3.2.zip
echo N | unzip ASIOSDK2.3.2.zip
echo

cp ASIOSDK2.3.2/common/asio.h .

sed -i 's/unsigned long/ULONG/g'                                       asio.h
sed -i 's/long long int/LONGLONG/g'                                    asio.h
sed -i 's/long int/LONG/g'                                             asio.h
sed -i 's/long/LONG/g'                                                 asio.h
sed -i 's/(\*bufferSwitch)/(CALLBACK *bufferSwitch)/g'                 asio.h
sed -i 's/(\*sampleRateDidChange)/(CALLBACK *sampleRateDidChange)/g'   asio.h
sed -i 's/(\*asioMessage)/(CALLBACK *asioMessage)/g'                   asio.h
sed -i 's/(\*bufferSwitchTimeInfo)/(CALLBACK *bufferSwitchTimeInfo)/g' asio.h

is_installed() {
    if [ -n "$(apt list -iqq $1 2>/dev/null)" ]; then
        echo 1
    else
        echo 0
    fi
}

libwine_package="libwine-dev"
libjack_package="libjack-jackd2-dev"

install_libwine=$(is_installed $libwine_package)
install_libjack=$(is_installed $libjack_package)

# install missing dependencies
if [ $install_libwine -eq 0 ]; then
    sudo apt install -qq --yes $libwine_package
fi

if [ $install_libjack -eq 0 ]; then
    sudo apt install -qq --yes $libjack_package
fi

make

# remove packages that were not installed at first
if [ $install_libwine -eq 0 ]; then
    sudo apt purge -qq --yes $libwine_package
fi

if [ $install_libjack -eq 0 ]; then
    sudo apt purge -qq --yes $libjack_package
fi
