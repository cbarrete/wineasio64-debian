# Information

This repository provides an easy way to install wineasio on 64-bit Debian
systems by automatically downloading the ASIO SDK, building wineasio,
registering the dll and installing it in a prefix. More information about
wineasio is available at https://github.com/jhernberg/wineasio.

# Installation

1. `$ ./build.sh`
2. `$ ./install.sh`

`apt` might ask for the root password in `build.sh` if some dependencies are not
yet installed. Any package installed by the script will be purged if the build
succeeds.

To install wineasio elsewhere than in the default Wine prefix (`~/.wine`), pass
the path in the environment as WINEPREFIX.

# Modifications

Besides wrapping the installation, the following modifications have been
performed:

- Addition of a required `include` path on Debian systems
- Use of `sed` instead of `ed` for the edition of `asio.h`
